var Poller = (function() {
    var Poller = Poller || {};
    var pollInterval = $('meta#poller-interval-seconds').attr('content') * 1000;

    Poller.poll = function() {
        if (!Poller.username || !Poller.issue) {
            return;
        }
        $.ajax({
            url : '/v0.1/issues/' + encodeURIComponent(Poller.issue)
                    + '/viewers',
            type : 'PUT',
            data : JSON.stringify({
                name : Poller.username
            }),
            dataType : 'json',
            contentType : 'text/json',

            success : function(data) {
                Poller.onSuccess(data);
            },

            error : function(jqXHR, textStatus, errorThrown) {
                Poller.onError(jqXHR, textStatus, errorThrown);
            }

        });
    }

    Poller.onSuccess = function() {
    };
    Poller.onError = function() {
    };

    Poller.username = null;
    Poller.issue = null;

    setInterval(Poller.poll, pollInterval);

    return Poller;
})();
