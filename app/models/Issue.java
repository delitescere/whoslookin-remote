package models;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import org.joda.time.DateTime;

import play.Play;

/**
 * Represents viewer information for a given issue.
 */
public class Issue {
    private String id;
    private Cache<String, Viewer> viewers =  CacheBuilder.newBuilder()
        .maximumSize(100)
        .expireAfterWrite(Long.valueOf(Play.configuration.getProperty("whoslookin.viewer-expiry.seconds", "120")), TimeUnit.SECONDS)
        .build();

    public Issue(final String id)
    {
        this.id = id;
    }

    public Collection<Viewer> getViewers()
    {
       return viewers.asMap().values();
    }

    public Issue addViewer(Viewer viewer)
    {
        viewer.lastSeen = DateTime.now().toString();
        viewers.put(viewer.name, viewer);
        return this;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Issue [id=").append(id).append(", viewers=").append(viewers.asMap()).append("]");
        return builder.toString();
    }

}
