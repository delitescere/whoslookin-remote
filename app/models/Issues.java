package models;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MapMaker;

/**
 * Dumb store for a collection of issues that have viewers.
 */
public class Issues
{
    private static Cache<String, Issue> cache = CacheBuilder.newBuilder()
                                                                .maximumSize(10000)
                                                                .expireAfterAccess(1, TimeUnit.HOURS)
                                                                .build();

    /**
     * @return viewer information for issue with the supplied id. The information is created with 0 viewers if it has not been accessed
     *         before.
     */
    public static Issue get(final String id) throws ExecutionException
    {
        return cache.get(id, new Callable<Issue>()
        {
            @Override
            public Issue call() throws Exception
            {
                return new Issue(id);
            }
        });
    }

    public static ImmutableMap<String, Issue> getAll()
    {
        return ImmutableMap.copyOf(cache.asMap());
    }

}
