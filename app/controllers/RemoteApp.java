package controllers;

import play.mvc.Controller;

public class RemoteApp extends Controller
{
    public static void root() {
        renderText("This is the Who's lookin' Remote App.");
    }

    public static void profile() {
        renderText("This is the Who's lookin' Remote App profile.");
    }


    public static void register() {
        renderTemplate("RemoteApp/register.xml");
    }
}
