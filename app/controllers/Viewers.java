package controllers;

import play.*;
import play.mvc.*;

import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import models.Issue;

import models.*;

public class Viewers extends Controller
{
    // Gson claims to be thread-safe.
    private static Gson gson = new GsonBuilder().create();

    public static void index()
    {
        render();
    }

    public static void addViewerToIssue(String id)
    {
        Issue issue;
        try
        {
            issue = Issues.get(id);
        }
        catch (ExecutionException e)
        {
            error(new IllegalStateException("Failed to retrieve viewer information for issue with key: %s", e));
            return;
        }

        try {
            Viewer newViewer = gson.fromJson(requestBodyReader(), Viewer.class);
            issue.addViewer(newViewer);
        } catch (Exception e) {
            error(400, "Could not extract viewer information from request: " + e);
            return;
        }

        renderJSON(issue.getViewers());
    }

    public static InputStreamReader requestBodyReader() throws UnsupportedEncodingException
    {
        return new InputStreamReader(request.body, request.encoding);
    }

    public static void listViewersForIssue(String id)
    {
        Issue issue;
        try
        {
            issue = Issues.get(id);
        }
        catch (ExecutionException e)
        {
            error(new IllegalStateException("Failed to retrieve viewer information for issue with key: %s", e));
            return;
        }

        Logger.info("Returning list of viewers for '%s'", id);
        renderJSON(issue.getViewers());
    }

    public static void audit() {
        StringBuilder sb = new StringBuilder();
        for (Entry<String, Issue> entry : Issues.getAll().entrySet()) {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        renderText(sb.toString());
    }


}