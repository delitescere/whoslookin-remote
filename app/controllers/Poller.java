package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Poller extends Controller {

    public static void index() {
        String pollTime = Play.configuration.getProperty("whoslookin.poll-time.seconds", "60");
        String jiraBase = request.headers.get("referer").value().split("/browse")[0]; //TODO there is a better way.
        render(pollTime, jiraBase);
    }

}